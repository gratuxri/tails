# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Al Shahrior Hasan Sagor <shahrior3814@gmail.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-09-13 20:10+0200\n"
"PO-Revision-Date: 2018-01-13 11:39+0000\n"
"Last-Translator: Al Shahrior Hasan Sagor <shahrior3814@gmail.com>\n"
"Language-Team: Bengali (Bangladesh) (http://www.transifex.com/otf/torproject/"
"language/bn_BD/)\n"
"Language: bn_BD\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:39
msgid "Tor is ready"
msgstr "টর প্রস্তুত"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:40
msgid "You can now access the Internet."
msgstr "আপনি এখন ইন্টারনেট অ্যাক্সেস করতে পারেন।"

#: config/chroot_local-includes/etc/whisperback/config.py:65
#, python-format
msgid ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>\n"
"Giving us an email address allows us to contact you to clarify the problem. "
"This\n"
"is needed for the vast majority of the reports we receive as most reports\n"
"without any contact information are useless. On the other hand it also "
"provides\n"
"an opportunity for eavesdroppers, like your email or Internet provider, to\n"
"confirm that you are using Tails.\n"
"</p>\n"
msgstr ""
"<h1> আপনার বাগ ঠিক করার জন্য আমাদের সাহায্য করুন! </ h1>\n"
"<p> <a href=\"%s\"> আমাদের বাগ রিপোর্টিং নির্দেশাবলী </a> পড়ুন। </ p>\n"
"<p> <strong> আরো ব্যক্তিগত তথ্য অন্তর্ভুক্ত করবেন না\n"
"প্রয়োজন </ strong> </ p &>\n"
"<h2> আমাদের একটি ইমেল ঠিকানা দেওয়ার বিষয়ে </ h2>\n"
"<P> যে\n"
"আমাদের একটি ইমেল ঠিকানা প্রদান আমাদের সমস্যা ব্যাখ্যা করতে আপনাকে যোগাযোগ করতে "
"পারবেন। এই\n"
"আমরা যে রিপোর্টগুলি পেয়েছি তার বেশিরভাগ রিপোর্টগুলির জন্য প্রয়োজন হয়\n"
"কোন যোগাযোগের তথ্য ব্যতীত অর্থহীন হয়। অন্যদিকে এটি এটি প্রদান করে\n"
"আপনার ইমেইল বা ইন্টারনেট প্রদানকারী, যেমন eavesdroppers জন্য একটি সুযোগ\n"
"আপনি পেরেক ব্যবহার করছেন তা নিশ্চিত করুন\n"
"? </ P>\n"

#: config/chroot_local-includes/usr/local/bin/electrum:17
msgid "Persistence is disabled for Electrum"
msgstr "ইস্ট্রামের জন্য স্থায়ীত্ব অক্ষম করা হয়েছে"

#: config/chroot_local-includes/usr/local/bin/electrum:19
msgid ""
"When you reboot Tails, all of Electrum's data will be lost, including your "
"Bitcoin wallet. It is strongly recommended to only run Electrum when its "
"persistence feature is activated."
msgstr ""
"যখন আপনি পুংগুলি পুনরায় চালু করবেন, তখন আপনার বিটকয়েন ওয়ালেট সহ সব ইলেক্ট্রামের "
"ডাটা হারিয়ে যাবে। এটি দৃঢ়ভাবে শুধুমাত্র ইন্টিগ্রেটে চালানোর জন্য সুপারিশ করা হয় যখন "
"তার দৃঢ়তা বৈশিষ্ট্য সক্রিয় করা হয়।"

#: config/chroot_local-includes/usr/local/bin/electrum:21
msgid "Do you want to start Electrum anyway?"
msgstr "আপনি ইলেক্ট্রাম যাই হোক না কেন শুরু করতে চান?"

#: config/chroot_local-includes/usr/local/bin/electrum:23
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:41
msgid "_Launch"
msgstr "_Launch"

#: config/chroot_local-includes/usr/local/bin/electrum:24
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:42
msgid "_Exit"
msgstr "_Exit"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/shutdown-helper@tails.boum.org/extension.js:71
msgid "Restart"
msgstr "পুনরায় চালু করুন"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/shutdown-helper@tails.boum.org/extension.js:74
msgid "Power Off"
msgstr "যন্ত্র বন্ধ"

#: config/chroot_local-includes/usr/local/bin/tails-about:22
#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:1
msgid "Tails"
msgstr "Tails"

#: config/chroot_local-includes/usr/local/bin/tails-about:25
#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:1
msgid "About Tails"
msgstr "Tails সম্পর্কে"

#: config/chroot_local-includes/usr/local/bin/tails-about:35
msgid "The Amnesic Incognito Live System"
msgstr "অ্যামেসিক ছদ্মবেশী লাইভ সিস্টেম"

#: config/chroot_local-includes/usr/local/bin/tails-about:36
#, python-format
msgid ""
"Build information:\n"
"%s"
msgstr ""
"তথ্য তৈরি করুন::\n"
"%s"

#: config/chroot_local-includes/usr/local/bin/tails-about:54
msgid "not available"
msgstr "পাওয়া যায় না"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:147
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:162
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:166
msgid "Your additional software"
msgstr "আপনার অতিরিক্ত সফ্টওয়্যার"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:148
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:167
msgid ""
"The upgrade failed. This might be due to a network problem. Please check "
"your network connection, try to restart Tails, or read the system log to "
"understand better the problem."
msgstr ""
"আপগ্রেড ব্যর্থ হয়েছে এটি একটি নেটওয়ার্ক সমস্যা কারণে হতে পারে। দয়া করে আপনার "
"নেটওয়ার্ক সংযোগ চেক করুন, টয়লেট পুনরায় আরম্ভ করার চেষ্টা করুন, বা সমস্যাটি ভালভাবে "
"বোঝার জন্য সিস্টেম লগটি পড়ুন।"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:163
msgid "The upgrade was successful."
msgstr "আপগ্রেড সফল হয়েছে।"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:52
msgid "Synchronizing the system's clock"
msgstr "সিস্টেমের ঘড়ি সমন্বয় করা"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:53
msgid ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."
msgstr ""
"টর সঠিকভাবে কাজ করার জন্য একটি সঠিক ঘড়ি প্রয়োজন, বিশেষ করে লুকানো পরিষেবাগুলির "
"জন্য অনুগ্রহপূর্বক অপেক্ষা করুন..."

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:87
msgid "Failed to synchronize the clock!"
msgstr "ঘড়ি সিঙ্ক্রোনাইজ করতে ব্যর্থ!"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:124
msgid "This version of Tails has known security issues:"
msgstr "পুচ্ছ এর এই সংস্করণ নিরাপত্তা বিষয় পরিচিত হয়েছে:"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:134
msgid "Known security issues"
msgstr "পরিচিত নিরাপত্তা বিষয়"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:52
#, sh-format
msgid "Network card ${nic} disabled"
msgstr "নেটওয়ার্ক কার্ড ${nic} অক্ষম"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:53
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}) so it is "
"temporarily disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"নেটওয়ার্ক কার্ড $ {nic_name} ($ {nic}) জন্য MAC স্পুফিং ব্যর্থ হয়েছে তাই এটি "
"অস্থায়ীভাবে অক্ষম করা হয়েছে।\n"
"আপনি পুচ্ছ পুনর্সূচনা এবং ম্যাক spoofing অক্ষম করতে পারে।"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:62
msgid "All networking disabled"
msgstr "সমস্ত নেটওয়ার্কিং নিষ্ক্রিয়"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:63
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}). The error "
"recovery also failed so all networking is disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"নেটওয়ার্ক কার্ড $ {nic_name} ($ {nic}) জন্য MAC স্পুফিং ব্যর্থ হয়েছে। ত্রুটি "
"পুনরুদ্ধারের ব্যর্থ তাই সমস্ত নেটওয়ার্কিং নিষ্ক্রিয় করা হয়।\n"
"আপনি পুচ্ছ পুনর্সূচনা এবং ম্যাক spoofing অক্ষম করতে পারে।"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:24
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:27
msgid "error:"
msgstr "ত্রুটি:"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:25
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:28
msgid "Error"
msgstr "ভুল"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:45
msgid ""
"<b>Not enough memory available to check for upgrades.</b>\n"
"\n"
"Make sure this system satisfies the requirements for running Tails.\n"
"See file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Try to restart Tails to check for upgrades again.\n"
"\n"
"Or do a manual upgrade.\n"
"See https://tails.boum.org/doc/first_steps/upgrade#manual"
msgstr ""
"<b> আপগ্রেডগুলির জন্য চেক করার জন্য যথেষ্ট মেমরি নেই। </ b>\n"
"\n"
"নিশ্চিত করুন যে এই সিস্টেম টাইল চালানোর জন্য প্রয়োজনীয়তা সন্তুষ্ট করুন\n"
"ফাইল দেখুন: ///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"আবার আপগ্রেডের জন্য পরীক্ষা করতে টাইল পুনরায় আরম্ভ করার চেষ্টা করুন।\n"
"\n"
"বা ম্যানুয়াল আপগ্রেড কি।\n"
"দেখুন https://tails.boum.org/doc/first_steps/upgrade#manual"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:71
msgid "Warning: virtual machine detected!"
msgstr "সতর্কতা: ভার্চুয়াল মেশিন সনাক্ত!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:73
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails."
msgstr ""
"উভয় হোস্ট অপারেটিং সিস্টেম এবং ভার্চুয়ালাইজেশন সফটওয়্যারটি আপনি কি টাইলগুলিতে "
"করছেন তা নিরীক্ষণ করতে সক্ষম।"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:76
msgid "Warning: non-free virtual machine detected!"
msgstr "সতর্কতা: অ-মুক্ত ভার্চুয়াল মেশিন সনাক্ত!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:78
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails. Only free software can be considered "
"trustworthy, for both the host operating system and the virtualization "
"software."
msgstr ""
"উভয় হোস্ট অপারেটিং সিস্টেম এবং ভার্চুয়ালাইজেশন সফটওয়্যারটি আপনি কি টাইলগুলিতে "
"করছেন তা নিরীক্ষণ করতে সক্ষম। হোস্ট অপারেটিং সিস্টেম এবং ভার্চুয়ালাইজেশন সফ্টওয়্যার "
"উভয়ের জন্য শুধুমাত্র ফ্রি সফ্টওয়্যার নির্ভরযোগ্য বলে মনে করা যেতে পারে।"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:83
msgid "Learn more"
msgstr "আরো জানুন"

#: config/chroot_local-includes/usr/local/bin/tor-browser:43
msgid "Tor is not ready"
msgstr "টর প্রস্তুত না"

#: config/chroot_local-includes/usr/local/bin/tor-browser:44
msgid "Tor is not ready. Start Tor Browser anyway?"
msgstr "টর প্রস্তুত না টর ব্রাউজার যাইহোক শুরু?"

#: config/chroot_local-includes/usr/local/bin/tor-browser:45
msgid "Start Tor Browser"
msgstr "টর ব্রাউজার শুরু করুন"

#: config/chroot_local-includes/usr/local/bin/tor-browser:46
msgid "Cancel"
msgstr "বাতিল"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:38
msgid "Do you really want to launch the Unsafe Browser?"
msgstr "আপনি কি সত্যিই অসুরক্ষিত ব্রাউজার চালু করতে চান?"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:40
msgid ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>.\\nOnly "
"use the Unsafe Browser if necessary, for example\\nif you have to login or "
"register to activate your Internet connection."
msgstr ""
"অনিরাপদ ব্রাউজারের মধ্যে নেটওয়ার্ক ক্রিয়াকলাপটি <b> বেনামী নয় </ b>। \\ N প্রয়োজন "
"হলে অনিরাপদ ব্রাউজার ব্যবহার করুন, উদাহরণস্বরূপ \\ n যদি আপনার ইন্টারনেট সংযোগ "
"সক্রিয় করার জন্য আপনাকে লগইন বা নিবন্ধন করতে হয়।"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:51
msgid "Starting the Unsafe Browser..."
msgstr "অসুরক্ষিত ব্রাউজার শুরু হচ্ছে ..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:52
msgid "This may take a while, so please be patient."
msgstr "এটি একটি সময় নিতে পারে, তাই ধৈর্য থাকুন।"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:57
msgid "Shutting down the Unsafe Browser..."
msgstr "অসুরক্ষিত ব্রাউজার বন্ধ করা হচ্ছে ..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:58
msgid ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."
msgstr ""
"এটি একটি সময় নিতে পারে, এবং আপনি সঠিকভাবে বন্ধ না হওয়া পর্যন্ত অনিরাপদ "
"ব্রাউজারটি পুনরায় চালু করতে পারবেন না।"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:70
msgid "Failed to restart Tor."
msgstr "টর পুনরায় আরম্ভ করতে ব্যর্থ হয়েছে"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:84
#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:1
msgid "Unsafe Browser"
msgstr "অসুরক্ষিত ব্রাউজার"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:92
msgid ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."
msgstr ""
"অন্য একটি অনিরাপদ ব্রাউজার বর্তমানে চলছে বা পরিষ্কার হচ্ছে। কিছুক্ষণের মধ্যে পুনরায় "
"চেষ্টা করুন।"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:104
msgid ""
"NetworkManager passed us garbage data when trying to deduce the clearnet DNS "
"server."
msgstr ""
"ক্লারনেট ডিএনএস সার্ভারটি বের করার চেষ্টা করার সময় নেটওয়ার্কে ম্যানেজারটি আমাদের "
"আবর্জনা উপাত্ত দিয়েছিল।"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:114
msgid ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."
msgstr ""
"কোনও DNS সার্ভার DHCP এর মাধ্যমে পাওয়া যায় না বা ম্যানুয়ালি কনফিগার করা হয়েছে "
"NetworkManager।"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:122
msgid "Failed to setup chroot."
msgstr "সেটআপ ক্রুটোতে ব্যর্থ হয়েছে"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:128
msgid "Failed to configure browser."
msgstr "ব্রাউজার কনফিগার করতে ব্যর্থ"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:133
msgid "Failed to run browser."
msgstr "ব্রাউজার চালাতে ব্যর্থ হয়েছে"

#: ../config/chroot_local-includes/etc/skel/Desktop/Report_an_error.desktop.in.h:1
msgid "Report an error"
msgstr "একটি ত্রুটি রিপোর্ট করুন"

#: ../config/chroot_local-includes/etc/skel/Desktop/tails-documentation.desktop.in.h:1
#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:1
msgid "Tails documentation"
msgstr "টাইল ডকুমেন্টেশন"

#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:2
msgid "Learn how to use Tails"
msgstr "Tails ব্যবহার কিভাবে শিখুন?"

#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:2
msgid "Learn more about Tails"
msgstr "Tails সম্পর্কে আরও জানুন"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:1
msgid "Tor Browser"
msgstr "টর ব্রাউজার"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:2
msgid "Anonymous Web Browser"
msgstr "নামবিহীন ওয়েব ব্রাউজার"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:2
msgid "Browse the World Wide Web without anonymity"
msgstr "গোপনীয়তা ছাড়াই ওয়ার্ল্ড ওয়াইড ওয়েব ব্রাউজ করুন"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:3
msgid "Unsafe Web Browser"
msgstr "অসুরক্ষিত ওয়েব ব্রাউজার"

#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:2
msgid "Tails specific tools"
msgstr "টাইলস নির্দিষ্ট সরঞ্জাম"

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.root-terminal.policy.in.h:1
msgid "To start a Root Terminal, you need to authenticate."
msgstr "একটি রুট টার্মিনাল শুরু করার জন্য, আপনি প্রমাণীকরণ প্রয়োজন।"
